/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modbusclienttcp;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.net.TCPMasterConnection;

/**
 *
 * @author andrzej
 */
public class ModbusExecutor {

    private TCPMasterConnection connection;

    public ModbusExecutor(String ip, int port) throws UnknownHostException {
        this(InetAddress.getByName(ip), port);
    }

    public ModbusExecutor(InetAddress address, int port) {
        connection = new TCPMasterConnection(address);
        connection.setPort(port);
    }

    public void executeTransaction(ModbusRequest request, final ModbusCallback callback) {
        ModbusTCPTransaction trans = null;
        try {
            connection.connect();
            trans = new ModbusTCPTransaction(connection);
            trans.setRequest(request);

            trans.execute();

            final ModbusResponse response = trans.getResponse();

            
            new Runnable() {
                @Override
                public void run() {
                    callback.valueReceived(response);
                }
            }.run();

        } catch (Exception ex) {
            callback.connectionFailed(ex.getLocalizedMessage());
        } finally {
//            if (connection != null) {
//                connection.close();
//            }
        }

    }

}
