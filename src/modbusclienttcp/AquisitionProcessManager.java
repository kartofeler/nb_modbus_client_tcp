package modbusclienttcp;

import net.wimpi.modbus.Modbus;

import java.util.List;

/**
 * Created by Andrzej on 2014-11-26.
 */
public class AquisitionProcessManager {

    private TagListSourceInterface tagSource;
    private List<ModbusSingleTag> tagList;

    public AquisitionProcessManager(TagListSourceInterface sourceInterface){
        this.tagSource = sourceInterface;
        loadTagList();
    }

    private void loadTagList(){
        tagList = tagSource.readTagList();
    }

    public void startAquire(){
        for(ModbusSingleTag tag : tagList) tag.startAquire();
    }

    public void stopAquire(){
        for(ModbusSingleTag tag : tagList) tag.stopAquire();
    }

    public List<ModbusSingleTag> getTagList() {
        return tagList;
    }
}
