/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modbusclienttcp;

import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrzej
 */
public class RequestInterceptor {
    private static RequestInterceptor instance;
    
    public static RequestInterceptor getInstance(){
        if(instance == null) instance = new RequestInterceptor();
        return instance;
    }
    
    private Map<String, ModbusExecutor> executors;
    
    private RequestInterceptor(){
        executors = new Hashtable<>();
    }
    
    public ModbusExecutor getSuitableExecutor(String ip, int port){
        ModbusExecutor executor = executors.get(ip);
        if(executor == null){
            try {
                executor = new ModbusExecutor(ip, port);
                executors.put(ip, executor);
            } catch (UnknownHostException ex) {
                Logger.getLogger(RequestInterceptor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return executor;
    }
    
}
