/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modbusclienttcp;

import java.util.Timer;
import java.util.TimerTask;
import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.msg.ReadCoilsRequest;
import net.wimpi.modbus.msg.ReadCoilsResponse;
import net.wimpi.modbus.msg.ReadInputDiscretesRequest;
import net.wimpi.modbus.msg.ReadInputDiscretesResponse;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;
import net.wimpi.modbus.msg.ReadMultipleRegistersRequest;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;

/**
 *
 * @author andrzej
 */
public class ModbusSingleTag implements ModbusCallback {

    public interface TagValueChangeCallback{
        public void valueChanged(int value);
        public void connectionFailed(String reason);
    }

    public final static int DIGITAL_OUTPUT = 0;
    public final static int DIGITAL_INPUT = 1;
    public final static int REGISTER_INPUT = 3;
    public final static int HOLDING_REGISTER = 4;
    public final static int OUT_OF_RANGE = -1;

    private int id;

    private String slaveAddress;
    private int slavePort;

    private String tagDescription;
    private int tagAddress;
    private int requestPeriod;
    
    private int parsedTagAddress;

    private Timer executeTimer;
    private ModbusRequest request;
    private ModbusResponse lastResponse;

    private TagValueChangeCallback valueCallback;

    public ModbusSingleTag(String slaveAddress, int slavePort, int tagAddress, int requestPeriod) throws Exception {
        this.slaveAddress = slaveAddress;
        this.slavePort = slavePort;
        this.tagAddress = tagAddress;
        this.requestPeriod = requestPeriod;

        switch (getAddressRangeCode(tagAddress)) {
            case DIGITAL_OUTPUT:
                parsedTagAddress = tagAddress;
                request = new ReadCoilsRequest(parsedTagAddress, 1);
                break;
            case DIGITAL_INPUT:
                parsedTagAddress = tagAddress - 10000;
                request = new ReadInputDiscretesRequest(parsedTagAddress, 1);
                break;
            case REGISTER_INPUT:
                parsedTagAddress = tagAddress - 30000;
                request = new ReadInputRegistersRequest(parsedTagAddress, 1);
                break;
            case HOLDING_REGISTER:
                parsedTagAddress = tagAddress - 40000;
                request = new ReadMultipleRegistersRequest(parsedTagAddress, 1);
                break;
            case OUT_OF_RANGE:
                throw new Exception("Address out of range");
        }
    }

    private int getAddressRangeCode(int address) {
        if (tagAddress >= 1 && tagAddress <= 10000) {
            return DIGITAL_OUTPUT;
        } else if (tagAddress >= 10001 && tagAddress <= 20000) {
            return DIGITAL_INPUT;
        } else if (tagAddress >= 30001 && tagAddress <= 40000) {
            return REGISTER_INPUT;
        } else if (tagAddress >= 40001 && tagAddress <= 50000) {
            return HOLDING_REGISTER;
        }
        return OUT_OF_RANGE;
    }

    private int getResponseValue() {
        if (lastResponse == null) {
            return -1;
        }
        switch (getAddressRangeCode(tagAddress)) {
            case DIGITAL_OUTPUT:
                return ((ReadCoilsResponse) lastResponse).getCoilStatus(0) ? 1 : 0;
            case DIGITAL_INPUT:
                return ((ReadInputDiscretesResponse) lastResponse).getDiscreteStatus(0) ? 1 : 0;
            case REGISTER_INPUT:
                return ((ReadInputRegistersResponse) lastResponse).getRegisterValue(0);
            case HOLDING_REGISTER:
                return ((ReadMultipleRegistersResponse) lastResponse).getRegisterValue(0);
        }
        return -1;
    }

    public void startAquire() {
        executeTimer = new Timer(true);
        executeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                ModbusExecutor exec = RequestInterceptor.getInstance().getSuitableExecutor(slaveAddress, slavePort);
                exec.executeTransaction(request, ModbusSingleTag.this);
            }
        }, 0, requestPeriod);
    }

    public void stopAquire() {
        executeTimer.cancel();
    }

    @Override
    public void valueReceived(ModbusResponse response) {
        this.lastResponse = response;
        if(valueCallback != null) valueCallback.valueChanged(getResponseValue());
    }

    @Override
    public void connectionFailed(String reason) {
        if(valueCallback != null) valueCallback.connectionFailed(reason);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlaveAddress() {
        return slaveAddress;
    }

    public int getSlavePort() {
        return slavePort;
    }

    public String getTagDescription() {
        return tagDescription;
    }

    public int getTagAddress() {
        return tagAddress;
    }

    public int getRequestPeriod() {
        return requestPeriod;
    }

    public ModbusResponse getLastResponse() {
        return lastResponse;
    }

    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription;
    }

    public void setValueCallback(TagValueChangeCallback valueCallback) {
        this.valueCallback = valueCallback;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.slaveAddress);
        builder.append(":");
        builder.append(this.slavePort);
        builder.append("     ");
        builder.append(this.tagAddress);
        builder.append("(");
        builder.append(this.requestPeriod);
        builder.append(") = ");
        builder.append(getResponseValue());
        builder.append("(");
        builder.append(this.tagDescription);
        builder.append(")");

        return builder.toString();
    }

}
