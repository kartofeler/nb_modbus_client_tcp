/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modbusclienttcp;

import java.io.File;
import java.util.List;

/**
 *
 * @author andrzej
 */
public interface TagListSourceInterface {
    public List<ModbusSingleTag> readTagList();
    public void writeTagList(List<ModbusSingleTag> list);
}
