/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modbusclienttcp;

import net.wimpi.modbus.msg.ModbusResponse;

/**
 *
 * @author andrzej
 */
public interface ModbusCallback {
    public void valueReceived(ModbusResponse response);
    public void connectionFailed(String reason);
}
